# References from Bankable

My reference from Bankable, [Angus Fergusson](https://www.linkedin.com/in/angusfergnz/). Angus is Bankable's Chief Customer Officer and I worked directly under him for Marketing and Sales. 

You can contact him directly by using Whatsapp at +44 7767 415354, but please be considerate of his time. 

